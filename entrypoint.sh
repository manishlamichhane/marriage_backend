#!/bin/bash


case $1 in
   "migrate_and_run")
      ./wait-for-it.sh --host=${DB_HOST} --port=${DB_PORT}
      ./manage.py makemigrations
      ./manage.py migrate
      ./manage.py runserver 0.0.0.0:9000
      ;;
    "run")
      ./manage.py runserver 0.0.0.0:9000
      ;;
    "makemigrations")
      ./manage.py makemigrations
      ;;
    "migrate")
      ./manage.py migrate
      ;;
    "djshell")
      ./manage.py shell
      ;;
    "test")
      pytest -v -x --no-migrations
      ;;
    "build-static")
      ./manage.py collectstatic --noinput
      ;;
  *)
     echo "Command not found. Running server by default"
     ./manage.py runserver 0.0.0.0:9000
     ;;
esac
