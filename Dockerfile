FROM python:3.8

ENV PYTHONUNBUFFERED=1
WORKDIR /marriage_backend

RUN pip install poetry
# installs dependency in the host os rather than inside a virtualenv
ENV POETRY_VIRTUALENVS_CREATE=false
ADD poetry.lock pyproject.toml /marriage_backend/
RUN poetry install

ADD . /marriage_backend/

ENTRYPOINT ["./entrypoint.sh"]
CMD [ "migrate_and_run" ]
