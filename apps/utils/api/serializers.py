from django_countries.serializer_fields import CountryField
from rest_framework import serializers


class UUIDMixin(metaclass=serializers.SerializerMetaclass):
    uuid = serializers.UUIDField(read_only=True)

    class Meta:
        fields = [
            'uuid',
        ]


class AddressMixin(metaclass=serializers.SerializerMetaclass):
    country = CountryField()
    city = serializers.CharField(required=False)
    postal_code = serializers.CharField(required=False)
    street_name = serializers.CharField(required=False)
    street_code = serializers.CharField(required=False)

    class Meta:
        fields = [
            'city',
            'country',
            'postal_code',
            'street_name',
            'street_code',
        ]


class ContactMixin(metaclass=serializers.SerializerMetaclass):
    phone_number = serializers.CharField(required=False)
    mobile_number = serializers.CharField(required=False)
    email = serializers.EmailField(required=False)

    class Meta:
        fields = [
            'phone_number',
            'mobile_number',
            'email',
        ]


class UUIDContactAddressMixin(UUIDMixin, AddressMixin, ContactMixin):
    class Meta:
        fields = UUIDMixin.Meta.fields + AddressMixin.Meta.fields + ContactMixin.Meta.fields
