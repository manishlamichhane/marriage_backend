from rest_framework import permissions


class CanCreateWithOutLogin(permissions.BasePermission):
    """Permits POST, all other request methods is permitted only if authenticated"""
    def has_permission(self, request, view):
        return request.method in ['POST', 'OPTIONS'] or request.user.is_authenticated
