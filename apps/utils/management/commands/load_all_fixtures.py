from django.core.management.base import BaseCommand
from django.core.management import call_command


class Command(BaseCommand):
    help = "Loads all fixtures in proper sequence"

    def handle(self, *args, **options):
        call_command("loaddata", "users", "games")
