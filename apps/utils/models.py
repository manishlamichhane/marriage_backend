import uuid

from django.db import models
from django_countries.fields import CountryField
from django.conf import settings


class UUIDMixin(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True, editable=False)

    class Meta:
        abstract = True


class CreatorUpdaterMixin(models.Model):
    creator = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True
    )

    class Meta:
        abstract = True


class CreatedUpdatedMixin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        abstract = True


class AddressMixin(models.Model):
    country = CountryField()
    city = models.TextField(null=True, blank=True)
    postal_code = models.TextField(null=True, blank=True)
    street_name = models.TextField(null=True, blank=True)
    street_code = models.TextField(null=True, blank=True)

    class Meta:
        abstract = True


class EmailMixin(models.Model):
    email = models.EmailField(null=True, blank=True)

    class Meta:
        abstract = True


class ContactMixin(models.Model):
    phone_number = models.TextField(null=True, blank=True)
    mobile_number = models.TextField(null=True, blank=True)

    class Meta:
        abstract = True


class EmailContactMixin(ContactMixin, EmailMixin):
    phone_number = models.TextField(null=True, blank=True)
    mobile_number = models.TextField(null=True, blank=True)

    class Meta:
        abstract = True


class BaseMixins(UUIDMixin, ContactMixin, AddressMixin, CreatedUpdatedMixin):
    pass

    class Meta:
        abstract = True


class UUIDCreatedUpdatedMixin(UUIDMixin, CreatedUpdatedMixin):
    pass

    class Meta:
        abstract = True


class BaseUUIDActorDatetimeMixin(UUIDMixin, CreatorUpdaterMixin, CreatedUpdatedMixin):
    pass

    class Meta:
        abstract = True
