from functools import cached_property
from django.db import models
from djchoices import DjangoChoices, ChoiceItem
from apps.utils.models import BaseUUIDActorDatetimeMixin, CreatedUpdatedMixin, UUIDMixin
from marriage_backend import settings


class CurrencyChoices(DjangoChoices):
    EUR = ChoiceItem("EUR", "EUR")
    NRP = ChoiceItem("NPR", "NPR")
    USD = ChoiceItem("USD", "USD")


class Game(BaseUUIDActorDatetimeMixin):
    name = models.CharField(unique=True, max_length=64)
    currency = models.TextField(choices=CurrencyChoices, default=CurrencyChoices.EUR)
    # 1 points equals to how many cents or 100th unit of the currency.
    # In case of Rupees it would be 10 paisa by default, In case of Euro it would be 10 cents
    point_rate = models.IntegerField(default=10)
    updater = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True,
        related_name="updated_game",
    )

    class Meta:
        verbose_name = "Game"

    def __str__(self):
        return f"{self.creator} {self.uuid} {self.currency}"


class Round(UUIDMixin, CreatedUpdatedMixin):
    game = models.ForeignKey(Game, on_delete=models.PROTECT, related_name="rounds")

    class Meta:
        verbose_name = "Round"

    def __str__(self):
        return f"{self.uuid}"

    @cached_property
    def shown_by(self):
        if self.players.count():
            return self.players.get(did_show=True)

    @cached_property
    def highest_scorer(self):
        if self.players.count():
            return self.players.order_by("-round_point").first()


class Player(UUIDMixin, CreatedUpdatedMixin):
    """A User in a Round is termed as a Player.

    So a Player is a User with Round specific details like:
    - did_show: (User and did_show has a unique combination per Round as only 1 player can "show" per Round)
    - has_seen_maal: If a user has seen maal in this round or not
    - played_dublee: If a user has played dublee or not
    - maal: Total maal a user has in this round
    - round_point: after all the deductions, round_point = maal * point_rate
    """

    round = models.ForeignKey(Round, on_delete=models.CASCADE, related_name="players")
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    did_show = models.BooleanField(default=False)
    has_seen_maal = models.BooleanField(default=False)
    played_dublee = models.BooleanField(default=False)
    maal = models.IntegerField(default=0)
    round_point = models.IntegerField(default=0)

    class Meta:
        unique_together = ["user", "did_show"]
        verbose_name = "Player"

    def __str__(self) -> str:
        return f"{self.user} Point: {self.round_point}"
