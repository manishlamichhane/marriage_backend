import pytest
from apps.user.tests.conftest import *
from apps.game.models import Game, Player, Round

from ddf import G


@pytest.fixture
def game(user):
    return G(Game, creator=user, updater=user, name="Game 1")


@pytest.fixture
def round(game):
    return G(Round, game=game)


@pytest.fixture
def player(round, user):
    return G(Player, user=user, round=round)
