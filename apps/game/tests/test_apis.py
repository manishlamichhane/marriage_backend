from django.urls import reverse
from rest_framework import status

from apps.game.models import Game


class TestGameAPI:
    def test_create_game_success(self, user_client, user):
        assert Game.objects.count() == 0
        url = reverse("api:v1:game:game-list")
        name = "Game 1"
        response = user_client.post(url, data={"name": name})

        assert response.status_code == status.HTTP_201_CREATED
        assert Game.objects.filter(creator=user, name=name).count() == 1
