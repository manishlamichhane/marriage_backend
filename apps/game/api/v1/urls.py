"""marriage_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from django.urls.conf import include
from django.views.generic import base
from rest_framework_nested import routers

from apps.game.api.v1 import views

app_name = "game"


router = routers.SimpleRouter()
router.register(r"game", views.Game, basename="game")

game_router = routers.NestedSimpleRouter(router, r"game", lookup="game")
game_router.register(r"round", views.Round, basename="round")
# 'basename' is optional. Needed only if the same viewset is registered more than once
# Official DRF docs on this option: http://www.django-rest-framework.org/api-guide/routers/

urlpatterns = [
    path(r"", include(router.urls)),
    path(r"", include(game_router.urls)),
    path("player/<uuid:uuid>", views.Player.as_view(), name="player-detail"),
]
