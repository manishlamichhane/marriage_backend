from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault

from apps.game.models import Game, Round, Player
from apps.user.models import User


class GameSerializer(serializers.ModelSerializer):
    creator = serializers.HiddenField(default=CurrentUserDefault())
    updater = serializers.HiddenField(default=CurrentUserDefault())
    uuid = serializers.UUIDField(read_only=True)
    point_rate = serializers.IntegerField(default=10)
    created_at = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Game
        fields = [
            "name",
            "currency",
            "creator",
            "point_rate",
            "updater",
            "updated_at",
            "created_at",
            "uuid",
        ]


class PlayerSerializer(serializers.ModelSerializer):
    user = serializers.SlugRelatedField(slug_field="uuid", queryset=User.objects)

    class Meta:
        model = Player
        fields = [
            "uuid",
            "user",
            "did_show",
            "has_seen_maal",
            "played_dublee",
            "maal",
            "round_point",
        ]


class RoundSerializer(serializers.ModelSerializer):
    round_number = serializers.IntegerField(source="id", read_only=True)
    game = serializers.SlugRelatedField(slug_field="uuid", queryset=Game.objects)
    users = serializers.SlugRelatedField(
        slug_field="uuid",
        queryset=User.objects.all(),
        write_only=True,
        many=True,
    )
    players = PlayerSerializer(many=True, read_only=True)

    class Meta:
        model = Round
        fields = [
            "uuid",
            "game",
            "users",
            "players",
            "round_number",
        ]

    def create(self, validated_data):
        users = validated_data.pop("users")
        round = super().create(validated_data)

        players = []
        for user in users:
            player = Player(user=user, round=round)
            players.append(player)

        Player.objects.bulk_create(players)
        return round
