from django.db.models import Q
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated

from apps.game.api.v1.serializers import (
    GameSerializer,
    PlayerSerializer,
    RoundSerializer,
)
from apps.game import models


class BaseAPIView(ModelViewSet):
    lookup_field = "uuid"
    permission_classes = [
        IsAuthenticated,
    ]


class Game(BaseAPIView):
    serializer_class = GameSerializer

    def get_queryset(self):
        user = self.request.user
        return models.Game.objects.filter(
            Q(creator=user) | Q(rounds__players__user=user)
        ).distinct()


class Round(BaseAPIView):
    serializer_class = RoundSerializer

    def get_queryset(self):
        return models.Round.objects.filter(game__uuid=self.kwargs["game_uuid"])


class Player(RetrieveUpdateAPIView):
    lookup_field = "uuid"
    serializer_class = PlayerSerializer

    def get_queryset(self):
        return models.Player.objects.filter(user=self.request.user)
