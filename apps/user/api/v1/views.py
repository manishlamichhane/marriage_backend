from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect
from django.urls import reverse

from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.user import models
from apps.user.api.v1 import serializers
from apps.utils.api.permissions import CanCreateWithOutLogin
from apps.utils.api.viewsets import CreateListUpdateDestroyViewSet, CreateListUpdateViewSet


class User(CreateListUpdateDestroyViewSet):
    """A ViewSet to Create Get Update and Delete user. User deletion deletes the profile as well"""
    lookup_field = 'uuid'
    queryset = models.User.objects
    serializer_class = serializers.User
    permission_classes = [CanCreateWithOutLogin, ]
    authentication_classes = []

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer(request.user)
        return Response(serializer.data)


class Profile(CreateListUpdateViewSet):
    """A ViewSet to Create Get Update profile. User deletion deletes the profile as well"""
    lookup_field = 'uuid'
    queryset = models.Profile.objects
    serializer_class = serializers.Profile
    permission_classes = [IsAuthenticated, ]

    def list(self, request, *args, **kwargs):
        if not request.user.profile:
            raise AttributeError('User has no profile')

        serializer = self.get_serializer(request.user.profile)
        return Response(serializer.data)


class Login(CreateAPIView):
    serializer_class = serializers.Login

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = authenticate(**serializer.data)

        if user is None:
            return Response(status=status.HTTP_403_FORBIDDEN)

        login(request, user)

        contract_list_page = reverse('api:v1:contract:contract-list')

        return redirect(contract_list_page)
