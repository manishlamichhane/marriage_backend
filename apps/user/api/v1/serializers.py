from rest_framework import serializers

from apps.user import models
from apps.utils.api.serializers import UUIDContactAddressMixin


class User(serializers.ModelSerializer):
    uuid = serializers.UUIDField(read_only=True)
    password = serializers.CharField(write_only=True)

    class Meta:
        model = models.User
        fields = [
                    'uuid',
                    'first_name',
                    'last_name',
                    'email',
                    'password',
                 ]

    def create(self, validated_data):
        plain_password = validated_data.pop("password")
        user = models.User(**validated_data)
        user.set_password(plain_password)
        user.save()
        
        return user

class Profile(UUIDContactAddressMixin, serializers.ModelSerializer):
    user = serializers.SlugRelatedField(queryset=models.User.objects.all(), slug_field='uuid')

    class Meta:
        model = models.Profile
        fields = ['user'] + UUIDContactAddressMixin.Meta.fields


class Login(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    class Meta:
        fields = [
            'email',
            'password',
        ]
