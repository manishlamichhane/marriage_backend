import pytest

from django.test import Client
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.test import APIClient

from apps.user.models import User


@pytest.fixture
def credentials():
    return {
        "email": "test@email.com",
        "password": "pass1234",
    }


@pytest.fixture
def user(credentials):
    return User.objects.create(**credentials)


@pytest.fixture
def access_token(user):
    """Needs to be provided in header of each request"""
    refresh = RefreshToken.for_user(user)

    return str(refresh.access_token)


@pytest.fixture
def refresh_token(user):
    refresh = RefreshToken.for_user(user)

    return str(refresh)


@pytest.fixture
def user_client(access_token):
    client = APIClient()
    client.credentials(HTTP_AUTHORIZATION='Bearer ' + access_token)
    return client
