import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from apps.user import models


class TestUserLoginAPI:
    @pytest.fixture
    def client(self):
        return APIClient()

    @pytest.fixture
    def user_api(self):
        return reverse("api:v1:user:user-list")

    def test_user_create(self, client, user_api, credentials):
        response = client.post(user_api, credentials)

        assert response.status_code == status.HTTP_201_CREATED

    def test_user_get(self, client, user_api, user):
        client.force_authenticate(user=user)

        response = client.get(user_api)
        assert response.status_code == status.HTTP_200_OK
        assert response.data["email"] == user.email

    def test_user_delete(self, client, user):
        client.force_authenticate(user=user)

        delete_api = reverse("api:v1:user:user-detail", kwargs={"uuid": user.uuid})

        response = client.delete(delete_api)
        assert response.status_code == status.HTTP_204_NO_CONTENT
