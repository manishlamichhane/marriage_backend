start:
	docker-compose up -d

stop:
	docker-compose stop

build:
	docker-compose up -d --build

build-static:
	# collects static files
	docker-compose run --rm web build-static

migrations:
	docker-compose run --rm web makemigrations

migrate: migrations
	docker-compose run --rm web migrate

test:
	# run pytest
	docker-compose run --rm web test

shell:
	# opens bash in a new throwaway web container
	docker-compose run --rm  --entrypoint /bin/bash web

django-shell:
	# opens django shell in a new throwaway container
	docker-compose run --rm web djshell

logs:
	docker-compose logs -f --tail=200

web-log:
	docker-compose logs -f web

db-log:
	docker-compose logs -f db

show-config:
	docker-compose config